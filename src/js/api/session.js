import { headers, parseJSON } from './utils';

export function postSession(email, password) {
  const options = {
    headers: headers(),
    method: 'POST',
    body: JSON.stringify({ email, password }),
  //  mode: 'no-cors'
  };

  return fetch('/api/sessions', options)
    .then(parseJSON);
}

export function postValidateToken(token) {
  const options = {
    headers: headers(),
     method: 'POST',
     body: JSON.stringify({token })
  };

  return fetch('/api/github/validate', options)
    .then(parseJSON);
}

export function postGithub() {
  const options = {
    headers: headers(),
    mode: 'no-cors'
    // method: 'POST',
    // body: JSON.stringify({ })
  };

  return fetch('/api/github', options)
    .then(parseJSON);
}
export function deleteSession(session) {
  const options = {
    headers: headers(),
    method: 'DELETE'
  };

  return fetch(session.uri, options)
    .then(parseJSON);
}
