import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';

import Split from 'grommet/components/Split';
import Sidebar from 'grommet/components/Sidebar';
import LoginForm from 'grommet/components/LoginForm';
import Article from 'grommet/components/Article';
import Section from 'grommet/components/Section';
import Heading from 'grommet/components/Heading';
import Paragraph from 'grommet/components/Paragraph';
import Footer from 'grommet/components/Footer';
import Logo from 'grommet/components/icons/Grommet';
import Button from "grommet/components/Button";
import SocialGithub from 'grommet/components/icons/base/SocialGithub';

import { login, githubLogin, validateToken } from '../actions/session';
import { navEnable } from '../actions/nav';
import { pageLoaded } from './utils';

class Login extends Component {
  constructor() {
    super();
    this._onSubmit = this._onSubmit.bind(this);
    this._onSubmitGithub = this._onSubmitGithub.bind(this);

  }

  componentDidMount() {
    pageLoaded('Login');
    if(window.location.search   ) {
      let params ={}; 
      window.location.search.slice( 1 ).split('&').forEach(s=>params[s.split('=')[0]]=s.split('=')[1]);
      const { router } = this.context;
      this.props.dispatch(validateToken(params.access_token,() => (
        router.history.push('/dashboard')
      )));
    }
    else {
      this.props.dispatch(navEnable(false));
    }
  }

  componentWillUnmount() {
    this.props.dispatch(navEnable(true));
  }

  _onSubmit(fields) {
    const { dispatch } = this.props;
    const { router } = this.context;
    dispatch(login(fields.username, fields.password, () => (
      router.history.push('/dashboard')
    )));
  }
  _onSubmitGithub() {
 //  const { dispatch } = this.props;
    const { router } = this.context;
    this.props.dispatch(githubLogin(() => (
      router.history.push('/dashboard')
    )));

  }
  render() {
    const { session: { error } } = this.props;

    return (
      <Split flex='left' separator={true}>

        <Article>
          <Section
            full={true}
            colorIndex='brand'
            texture='url(img/splash.png)'
            pad='large'
            justify='center'
            align='center'
          >
            <Heading tag='h1' strong={true}>Github Anomaly</Heading>
            <Paragraph align='center' size='large'>
              Development with Grommet is cool.
            </Paragraph>
          </Section>
        </Article>

        <Sidebar justify='between' align='center' pad='none' size='large'>
          <span />
          <LoginForm
            align='start'
            logo={<Logo className='logo' colorIndex='brand' />}
            title='Github Anomaly'
            onSubmit={this._onSubmit}
            errors={[error]}
            usernameType='text'
          />
          <Button icon={<SocialGithub />}
            label='Github'
         //   onClick={this._onSubmitGithub}
            href='/api/login' />
          <Footer
            direction='row'
            size='small'
            pad={{ horizontal: 'medium', vertical: 'small' }}
          >
            <span className='secondary'>&copy; 2018 Maty zisserman</span>
          </Footer>
        </Sidebar>

      </Split>
    );
  }
}

Login.defaultProps = {
  session: {
    error: undefined
  }
};

Login.propTypes = {
  dispatch: PropTypes.func.isRequired,
  session: PropTypes.shape({
    error: PropTypes.string
  })
};

Login.contextTypes = {
  router: PropTypes.object.isRequired,
};

const select = state => ({
  session: state.session
});

export default connect(select)(Login);
