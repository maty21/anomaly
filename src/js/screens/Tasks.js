import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import SocialGithub from 'grommet/components/icons/base/SocialGithub';
import Anchor from 'grommet/components/Anchor';
import Article from 'grommet/components/Article';
import Box from 'grommet/components/Box';
import Columns from 'grommet/components/Columns';
import Header from 'grommet/components/Header';
import Heading from 'grommet/components/Heading';

import Label from 'grommet/components/Label';
import List from 'grommet/components/List';
import ListItem from 'grommet/components/ListItem';
import Notification from 'grommet/components/Notification';
import Meter from 'grommet/components/Meter';
import Paragraph from 'grommet/components/Paragraph';
import Value from 'grommet/components/Value';
import Spinning from 'grommet/components/icons/Spinning';
import { getMessage } from 'grommet/utils/Intl';
import styled from "styled-components";
import NavControl from '../components/NavControl';
//import mfb from "react-mfb";
import { ChildButton, MainButton, Menu } from "@rzymek/react-mfb";
import {
  loadTasks, unloadTasks
} from '../actions/tasks';

import { pageLoaded } from './utils';

const StyledChlidFabButton = styled(ChildButton)`
background-color:#501eb4;
color:white;
`

const StyledMainFabButton = styled(MainButton)`
background-color:#501eb4;
color:white;
`

const ColumnBox = styled.div`
display:flex
flexDirection:column
`

const HeaderBox = styled.div`
height:65vh

`
const FooterBox = styled.div`
height:35vh
`
class Tasks extends Component {
  componentDidMount() {
    pageLoaded('Tasks');
    this.props.dispatch(loadTasks());
  }

  componentWillUnmount() {
    this.props.dispatch(unloadTasks());
  }

  render() {

    return (
      <ColumnBox>
        <HeaderBox>
             <Article primary={true}>
        <Header
          direction='row'
          justify='between'
          size='large'
          pad={{ horizontal: 'medium', between: 'small' }}
        > 
         <NavControl name={ 'Tasks'} />
         </Header>
         </Article> 
        </HeaderBox>
        <FooterBox>

        </FooterBox>
        <Menu effect={'zoomin'} method={'hover'} position={'br'}>
          <MainButton style={{ backgroundColor: '#501eb4', color: 'white' }} iconResting="ion-plus-round" iconActive="ion-close-round" />
          <StyledChlidFabButton style={{ color: 'white' }}
            //onClick={function(e){ console.log(e); e.preventDefault(); }}
            icon="ion-social-github"
            label="View on Github"
            href="https://github.com/nobitagit/react-material-floating-button/" />
          <StyledChlidFabButton style={{ color: 'white' }}
            icon="ion-social-octocat"
            label="Follow me on Github"
            href="https://github.com/nobitagit" />
          <StyledChlidFabButton style={{ color: 'white' }}
            icon="ion-social-twitter"
            label="Share on Twitter"
            href="http://twitter.com/share?text=Amazing Google Inbox style material floating menu as a React component!&url=http://nobitagit.github.io/react-material-floating-button/&hashtags=material,menu,reactjs,react,component" />
        </Menu>
      </ColumnBox >
      
    );
  }
}

Tasks.defaultProps = {
  error: undefined,
  tasks: []
};

Tasks.propTypes = {
  dispatch: PropTypes.func.isRequired,
  error: PropTypes.object,
  tasks: PropTypes.arrayOf(PropTypes.object)
};

Tasks.contextTypes = {
  intl: PropTypes.object
};

const select = state => ({ ...state.tasks });

export default connect(select)(Tasks);
